# Dev operations in sites

A customized Midnight Commander user menu tailored for operations inside a shared hosting environment.

### DISCLAIMER:
This method of working in shared hoosting environment is intented only for improving the way a root user performs. The available operations are still **VERY DANGEROUS** because they may affect the whole system.
**Use this at your own risk!**

### Usage:
The project affects the way Midnight Commander works. Just copy the "menu" file inside /root/.config/mc/. After a logout/login, the user menu (F2) will be changed to offer some site-specific operations.

### Capabilities:
The automation affects the following special functionality:
1.  Copy current item(s) to the side panel and chown it to destination owner:group.
2.  Move current item(s) to the side panel and chown it to destination owner:group.
3.  Create a file in current directory and chown it to current directory's owner:group.
4.  Create a directory in current directory and chown it to current directory's owner:group.
5.  Backup the current Wordpress site (files and database). Sensitive information such as web user, database user and password, etc are not transfered to the backup file. The derived .tar.gz backup file tries to keep up with the CPanel backup file structure.

### Parameters:
1. BACKDIR=/path/to/backup/directory # Wordpress backup directory
2. BASEURL="https://dev.example.com/backup" # Base URL for downloading the packed site (NO TRAILING SLASH!)